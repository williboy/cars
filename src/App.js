import React, { Component } from 'react';
import Mycars from './components/Mycars';
import './App.css';

class App extends Component{
   
  state = {
    titre: 'Mon catalogue voitures'
  }

  changeTitle = (e) => {
    this.setState({
      titre: 'Mon nouveau titre'
    })
  }


  render() {
  return (
    <div className="App">
         <Mycars  title={this.state.titre}/>
    </div>
  );
  }
}

export default App;
